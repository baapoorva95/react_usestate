import React,{ useState,useReducer} from 'react';
import './App.css';

function App() {
  const [,,com]=["pen","pencil","eraser"]
  console.log(com);
  const a = useState("happy");
  console.log(a);
  const [emotion,setEmotion] = useState("happy");
  const [mode,setMode] = useReducer(
    (mode)=>!mode,
    true
  );
  return (
    <div className="App">
        <h1>Current emotion is {emotion}</h1>
        <button onClick={() => setEmotion("Sad")}>make me sad</button>
        
      <button onClick={() => setEmotion("frustrated")}>make me frustrated</button><br></br>
      <input type="checkbox" value={mode} onChange={setMode}/>
      Darkmode
      <p>{mode?"Darkmode":"lightMode"}</p>
    </div>
  );
}

export default App;
